<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');



add_filter( 'embed_oembed_html', function( $html, $url, $attr, $post_ID ) {
    // Use $html, $url, $attr and $post_ID here if you like.

    // Wrap the HTML output.
    $html = '<div class="embed-responsive embed-responsive-16by9">' . $html . '</div>';

    // Return $html.
    return $html;
}, 10, 4 );

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array('page_title' => 'Opciones del sitio', 'menu_title' => 'Opciones del sitio'));

}

function get_youtube_ID($url) {

  preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
  $youtube_id = $match[1];

  return $youtube_id;
}


