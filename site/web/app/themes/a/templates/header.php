<header class="">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
<!--                <img class="logo-gob" src="--><?//= get_template_directory_uri() . '/dist/images/logo-gob-outline.png'; ?><!--">-->
            </div>
            <div class="col-md-6 text-align-center">
                <a href="/"><img class="logo-main" src="<?= get_template_directory_uri() . '/dist/images/logo-main.png'; ?>"></a>

            </div>
        </div>
    </div>
</header>

<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
                data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false"
                aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>

        </button>
        <!--    <a class="navbar-brand" href="--><?php //home_url('/') ?><!--"><img src="-->
        <? //= get_template_directory_uri() . '/dist/images/logo-bn.png'; ?><!--" alt="#BuenasNoticias"></a>-->
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="/"><span class="cat-ico icono-inicio"></span>Inicio</a>
                </li>
                <li class="nav-item dropdown">

                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="cat-ico icono-categorias"></span>
                        Categorías
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">

                    <a class="nav-link" href="/tema/mujeres/"><span class="cat-ico icono-mujeres"></span>Mujeres</a>
                </li>
                <li class="nav-item">

                    <a class="nav-link" href="/tema/bolsillo/"><span class="cat-ico icono-bolsillo"></span>Alivia tu bolsillo</a>
                </li>

            </ul>
        </div>
    </div>
</nav>
