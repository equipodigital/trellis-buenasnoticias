<?php
    $sabias_texto = get_field('texto_sabias_que', 'option');
    $sabias_url = get_field('url_sabias_que', 'options');

?>
<div class="block-sabias-que">
    <span class="check"></span>
    <h5>¿Sabías que...?</h5>
    <p><?php echo $sabias_texto ?></p>
    <a href="<?php echo $sabias_url ?>">Ver más</a>
</div>