<footer class="content-info">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <img class="logo-gob" src="<?= get_template_directory_uri() . '/dist/images/logo-gob-outline.png'; ?>">
            </div>
            <div class="col-md-6 text-align-center">
                <a href="#" class="logo-main"><img class="" src="<?= get_template_directory_uri() . '/dist/images/logo-main-alt.png'; ?>"></a>

            </div>
        </div>
    </div>
</footer>
