<?php while (have_posts()) : the_post(); ?>
    <article <?php post_class('post-body'); ?>>
        <div class="post-aside-meta">
            <date class="post-fecha">
                <p>25 de septiembre de 2017</p>
            </date>


        </div>
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <div class="social-share">
            <a href="#" class="social-share-fb"><i class="fa fa-facebook" aria-hidden="true"></i> Compartir en Facebook</a>
            <a href="#" class="social-share-tw"><i class="fa fa-twitter" aria-hidden="true"></i> Compartir en
                Twitter</a>
            <a href="#" class="social-share-wa"><i class="fa fa-whatsapp" aria-hidden="true"></i> Compartir por WhatsApp</a>
            <a href="#" class="social-share-mail"><i class="fa fa-envelope-o" aria-hidden="true"></i> Enviar vía
                mail</a>
        </div>


        <?php the_post_thumbnail(); ?>

        <div class="entry-content">
            <?php the_content(); ?>
        </div>

        <div class="clearfix"></div>


        <div class="row no-gutters categorias-lista categorias-lista-single">

            <?php $tags = wp_get_post_tags( get_the_ID()); ?>

            <?php foreach ($tags as $tag): ?>
                <div class="col-sm-4 item-categorias"><a href="<?php echo get_term_link($tag->term_id); ?>"><?php echo $tag->name ?></a></div>
            <?php endforeach ?>

            

        </div>

        <div class="social-share">
            <a href="#" class="social-share-fb"><i class="fa fa-facebook" aria-hidden="true"></i> Compartir en Facebook</a>
            <a href="#" class="social-share-tw"><i class="fa fa-twitter" aria-hidden="true"></i> Compartir en
                Twitter</a>
            <a href="#" class="social-share-wa"><i class="fa fa-whatsapp" aria-hidden="true"></i> Compartir por WhatsApp</a>
            <a href="#" class="social-share-mail"><i class="fa fa-envelope-o" aria-hidden="true"></i> Enviar vía
                mail</a>
        </div>
    </article>
<?php endwhile; ?>
