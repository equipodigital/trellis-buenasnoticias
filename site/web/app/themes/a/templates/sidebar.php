<aside class="post-sidebar">


    <div class="post-aside-mas-noticias noticias">
        <span class="cat-title">Más Noticias</span>

        <?php
        $args     = array(
            'posts_per_page' => 3,
        );
        $i        = 1;
        $noticias = new WP_Query($args);
        if ($noticias->have_posts()) : while ($noticias->have_posts()): $noticias->the_post(); ?>

            <a href="<?php the_permalink(); ?>">
                <article>
                    <div class="noticia-img">
                        <?php the_post_thumbnail('thumbnail',
                            ['class' => 'img-fluid responsive--full', 'title' => 'Feature image']); ?>
                        
                    </div>
                    <h3><?php the_title() ?></h3>
                </article>
            </a>


            <?php $i++; endwhile;
            wp_reset_postdata();
        endif; ?>
    </div>

    <div class="post-aside-categorias">
        <div class="cat-destacada cat-destacada-first">
            <div class="cat-destacada-header">
                <span class="cat-icono-mujeres"></span>
                <h2>Mujeres</h2>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur deleniti dicta dolores earum
                et, eveniet expedita laborum libero minima, molestiae, mollitia non nulla odit praesentium quaerat qui
                rerum sit!</p>
        </div>
        <div class="cat-destacada cat-destacada-last">
            <div class="cat-destacada-header">
                <span class="cat-icono-bolsillo"></span>
                <h2>Alivia tu bolsillo</h2>
            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consectetur deleniti dicta dolores earum
                et, eveniet expedita laborum libero minima, molestiae, mollitia non nulla odit praesentium quaerat qui
                rerum sit!</p>
        </div>

        <div class="cat-destacada cat-destacada-lista">
            <div class="cat-destacada-header">
                <span class="cat-icono-tags"></span>
                <h2>Categorías</h2>
            </div>
            <?php get_template_part('templates/partials', 'tag-list'); ?>
        </div>


    </div>
    <?php get_template_part('templates/partials', 'sabias-que'); ?>
    <?php // dynamic_sidebar('sidebar-primary'); ?>

</aside>
