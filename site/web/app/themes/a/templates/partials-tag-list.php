<div class="row no-gutters categorias-lista">
    <?php
    $tags = get_terms( array(
        'taxonomy' => 'post_tag',
        'hide_empty' => false,
    ) );
    foreach($tags as $tag):
        ?>

        <div class="col-sm-<?php echo (is_home()) ? '4' : '6'; ?> item-categorias"><a href="<?php echo get_term_link($tag->term_id) ?>"><?php echo $tag->name ?></a></div>
    <?php endforeach; ?>

</div>