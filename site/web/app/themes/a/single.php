<div class="container">
    <div class="row">


        <div class="col-md-8">
            <?php get_template_part('templates/content-single', get_post_type()); ?>
        </div>
        <div class="col-md-4">
            <?php get_template_part('templates/sidebar', get_post_type()); ?>
        </div>
    </div>



</div>
