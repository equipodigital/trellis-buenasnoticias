<?php 
    use Roots\Sage\Extras;
?>
<section class="destacados">
    <div class="container">
        <div class="row no-gutters">
            <span class="cat-title">#BuenasNoticias</span>

            <?php
            $noticias_destacadas = get_field('noticias_destacadas', 'options');
            $i = 0;
            $destacados = array();
            foreach ($noticias_destacadas as $noticia_destacada): ?>
                <?php if ($i === 0): ?>
                    <div class="col-sm-8">
                        <a href="<?php echo get_permalink($noticia_destacada['entrada']); ?>">
                            <article class="articulo-lg">

                                <?php echo get_the_post_thumbnail($noticia_destacada['entrada'], 'post-thumbnail',
                                    ['class' => 'img-fluid responsive--full', 'title' => 'Feature image']); ?>
                                <div class="post-info">
                                    <!--                                    <span class="compartido"><i class="fa fa-share-alt"></i> Compartido 45 veces</span>-->
                                    <h2><?php echo get_the_title($noticia_destacada['entrada']); ?></h2>
                                </div>

                            </article>
                        </a>
                    </div>

                <?php else: ?>
                    <?php if ($i === 1): ?>
                        <div class="col-sm-4">
                    <?php endif ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <a href="<?php echo get_permalink($noticia_destacada['entrada']); ?>">
                                <article class="articulo-sm">

                                    <?php echo get_the_post_thumbnail($noticia_destacada['entrada'], 'post-thumbnail',
                                        ['class' => 'img-fluid responsive--full', 'title' => 'Feature image']); ?>
                                    <div class="post-info">
<!--                                        <span class="compartido"><i-->
<!--                                                    class="fa fa-share-alt"></i> Compartido 24 veces</span>-->
                                        <h2><?php echo get_the_title($noticia_destacada['entrada']) ?></h2>
                                    </div>

                                </article>
                            </a>
                        </div>
                    </div>


                    <?php if ($i === 2): ?>
                        </div>
                    <?php endif ?>
                <?php endif ?>
            <?php $destacados[] = $noticia_destacada['entrada']; $i++; endforeach; ?>
        </div>
    </div>
</section>

<section class="play">
    <div class="container">
        <span class="cat-title">Dale play!</span>
        <div class="row">
            <div class="col-md-8">
                <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/<?php echo Extras\get_youtube_ID(get_field('video_destacado_url', 'options'))  ?>' frameborder='0' allowfullscreen></iframe></div>
            </div>

            <div class="col-md-4">
                <h2><?php echo get_field('video_destacado_titulo', 'options') ?></h2>
                <p><?php echo get_field('video_destacado_texto', 'options') ?></p>

                <a href="<?php echo get_field('video_destacado_link', 'options') ?>">Ver más</a>
            </div>

        </div>
    </div>
</section>

<section class="noticias">
    <div class="container">
        <span class="cat-title">Más Noticias</span>
        <div class="row">

            <?php
            $args     = array(
                'posts_per_page' => 6,
                'post__not_in' => $destacados
            );
            $i        = 1;
            $noticias = new WP_Query($args);
            if ($noticias->have_posts()) : while ($noticias->have_posts()): $noticias->the_post(); ?>
                <div class="col-sm-4">
                    <a href="<?php the_permalink(); ?>">
                        <article>
                            <div class="noticia-img">
                                <?php the_post_thumbnail('thumbnail',
                                    ['class' => 'img-fluid responsive--full', 'title' => 'Feature image']); ?>
<!--                                 <span class="compartido"><i class="fa fa-share-alt"></i> Compartido 45 veces</span>
 -->                            </div>
                            <h3><?php the_title() ?></h3>
                        </article>
                    </a>
                </div>

                <?php if (($i % 3) === 0): ?>
                    <div class="clearfix"></div>
                <?php endif; ?>
                <?php $i++; endwhile;
                wp_reset_postdata();
            endif; ?>


        </div>
        <a href="#" class="mas-noticias text-center">Más buenas noticias <i class="fa fa-chevron-down"></i></a>
    </div>
</section>

<section class="calendario">
    <div class="container">
        <span class="cat-title">¡Que no se te pase!</span>
        <div class="row no-gutters">
            <div class="col-sm-12">
                <?php get_template_part('templates/partials', 'calendar'); ?>
            </div>
         
        </div>
    </div>
</section>

<section class="redes-sociales">
    <div class="container">
        <span class="cat-title">Buenas Noticias en las Redes Sociales</span>
        <div class="row">
            <div class="col-sm-4">
                <h3 class="text-center"><i class="fa fa-facebook-official"></i> Facebook</h3>
                <div class="fb-page" data-href="https://www.facebook.com/Segegob/" data-tabs="timeline"
                     data-height="550" data-small-header="true" data-adapt-container-width="true"
                     data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/Segegob/" class="fb-xfbml-parse-ignore"><a
                                href="https://www.facebook.com/Segegob/">Secretaría General de Gobierno de Chile</a>
                    </blockquote>
                </div>
            </div>

            <div class="col-sm-4">
                <h3 class="text-center"><i class="fa fa-twitter"></i> Twitter</h3>
                <!-- <a class="twitter-timeline" data-height="550" data-dnt="true" href="https://twitter.com/segegob">Tweets
                    by segegob</a>
                <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> -->
                <?php echo get_field('embed_twitter', 'options') ?>
            </div>

            <div class="col-sm-4">
                <h3 class="text-center"><i class="fa fa-instagram"></i> Instagram</h3>

                <?php echo get_field('embed_instagram', 'options') ?>
            </div>
        </div>
    </div>
</section>

<section class="info-categorias">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 first-col">
                <?php
                $cat_mujeres = get_category_by_slug('mujeres');
                $cat_bolsillo = get_category_by_slug('bolsillo');
                ?>
                <ul class="list-unstyled">
                    <li class="media">
                        <img class="d-flex mr-3"
                             src="<?= get_template_directory_uri() . '/dist/images/cat-label-mujer.png'; ?>"
                             alt="Mujeres">
                        <div class="media-body">
                            <a href="<?php echo get_category_link($cat_mujeres->cat_ID) ?>"><h3 class="mt-0 mb-1">Mujeres</h3></a>
                            <p><?php ; echo $cat_mujeres->description; ?></p>
                        </div>
                    </li>
                    <li class="media my-4">
                        <img class="d-flex mr-3"
                             src="<?= get_template_directory_uri() . '/dist/images/cat-label-bolsillo.png'; ?>"
                             alt="Alivia tu bolsillo">
                        <div class="media-body">
                            <a href="<?php echo get_category_link($cat_mujeres->cat_ID) ?>"><h3 class="mt-0 mb-1">Alivia tu bolsillo</h3></a>
                            <p><?php echo $cat_bolsillo->description; ?></p>
                        </div>
                    </li>

                </ul>
            </div>
            <div class="col-sm-6 last-col">

                <div class="row">
                    <div class="media">
                        <img class="d-flex mr-3"
                             src="<?= get_template_directory_uri() . '/dist/images/cat-label-tags.png'; ?>"
                             alt="Generic placeholder image">
                        <div class="media-body">
                            <h3 class="mt-0">Buenas Noticias por Categoría</h3>
                        </div>
                    </div>

                    <?php get_template_part('templates/partials', 'tag-list'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
